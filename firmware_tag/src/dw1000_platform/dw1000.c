/*
 * Copyright (c) 2020 jindongsheng1024@163.com
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <string.h>
#include <device.h>
#include <drivers/spi.h>
#include <drivers/gpio.h>

#include "dw1000.h"
#include "deca_device_api.h"

#define DW1000_LP_MODE   1

#if DW1000_LP_MODE
/* Default communication configuration. See NOTE 1 below. */
static dwt_config_t config = {
    2,                   /* Channel number. */
    DWT_PRF_16M,         /* Pulse repetition frequency. */
    DWT_PLEN_1024,       /* Preamble length. Used in TX only. */
    DWT_PAC16,           /* Preamble acquisition chunk size. Used in RX only. */
    3,                   /* TX preamble code. Used in TX only. */
    3,                   /* RX preamble code. Used in RX only. */
    0,                   /* 0 to use standard SFD, 1 to use non-standard SFD. */
    DWT_BR_6M8,          /* Data rate. */
    DWT_PHRMODE_STD,     /* PHY header mode. */
    (1024 + 1 + 8 - 16)  /* SFD timeout (preamble length + 1 + SFD length - PAC size). Used in RX only. */
};
#else
/* Default communication configuration. We use here EVK1000's default mode (mode 3). */
static dwt_config_t config = {
    2,               /* Channel number. */
    DWT_PRF_64M,     /* Pulse repetition frequency. */
    DWT_PLEN_1024,   /* Preamble length. Used in TX only. */
    DWT_PAC32,       /* Preamble acquisition chunk size. Used in RX only. */
    9,               /* TX preamble code. Used in TX only. */
    9,               /* RX preamble code. Used in RX only. */
    1,               /* 0 to use standard SFD, 1 to use non-standard SFD. */
    DWT_BR_110K,     /* Data rate. */
    DWT_PHRMODE_STD, /* PHY header mode. */
    (1025 + 64 - 32) /* SFD timeout (preamble length + 1 + SFD length - PAC size). Used in RX only. */
};
#endif /* DW1000_LP_MODE */

static const struct device *g_dw1000_spi_dev = NULL;
static struct spi_config g_dw1000_spi_config;
static const struct device *g_dw1000_spi_cs_dev = NULL;

static const struct device *g_dw1000_reset_dev = NULL;
#define DW1000_RST_PIN    11

static int dw1000_spi_init(void)
{
    g_dw1000_spi_dev = device_get_binding("SPI_2");
    if (!g_dw1000_spi_dev) {
        printk("Can't fined SPI_1\n");
        return -ENODEV;
    }

    g_dw1000_spi_cs_dev = device_get_binding("GPIOB");
    if (!g_dw1000_spi_cs_dev) {
        printk("Can't fined GPIOB\n");
        return -ENODEV;
    }

    g_dw1000_spi_config.frequency = 1000000;
    g_dw1000_spi_config.operation = SPI_OP_MODE_MASTER |
        SPI_WORD_SET(8) | SPI_TRANSFER_MSB;
    g_dw1000_spi_config.slave = 0;
    g_dw1000_spi_config.cs = NULL;

    return 0;
}

int dw1000_spi_write(uint8_t *buf, uint32_t len)
{
    const struct spi_config *spi_cfg = &g_dw1000_spi_config;
    const struct spi_buf tx_buf = {
        .buf = buf,
        .len = len,
    };
    const struct spi_buf_set tx = {
        .buffers = &tx_buf,
        .count = 1
    };

    if (spi_write(g_dw1000_spi_dev, spi_cfg, &tx)) {
        return -EIO;
    }

    return 0;
}

int dw1000_spi_read(uint8_t *tx_buf_ptr, uint32_t tx_len, uint8_t *rx_buf_ptr, uint32_t rx_len)
{
    const struct spi_config *spi_cfg = &g_dw1000_spi_config;
	const struct spi_buf tx_buf = {
			.buf = tx_buf_ptr,
			.len = tx_len,
	};
	const struct spi_buf_set tx = {
		.buffers = &tx_buf,
		.count = 1
	};
	const struct spi_buf rx_buf[2] = {
		{
			.buf = NULL,
			.len = tx_len,
		},
		{
			.buf = rx_buf_ptr,
			.len = rx_len,
		}
	};
	const struct spi_buf_set rx = {
		.buffers = rx_buf,
		.count = 2,
	};

	if (spi_transceive(g_dw1000_spi_dev, spi_cfg, &tx, &rx)) {
		return -EIO;
	}

    return 0;
}

static int dw1000_gpio_init(void)
{
    g_dw1000_reset_dev = device_get_binding("GPIOA");
    if (!g_dw1000_reset_dev) {
        printk("Can't fine GPIOA\n");
        return -ENODEV;
    }

    gpio_pin_configure(g_dw1000_reset_dev, DW1000_RST_PIN, GPIO_OUTPUT_ACTIVE);

    return 0;
}

void dw1000_reset(void)
{
    k_usleep(10);
    gpio_pin_set(g_dw1000_reset_dev, DW1000_RST_PIN, 0);
    k_usleep(10);
    gpio_pin_set(g_dw1000_reset_dev, DW1000_RST_PIN, 1);
    k_sleep(K_MSEC(2000));
}

int dw1000_init(void)
{
    int ret;

    ret = dw1000_spi_init();
    if (ret < 0) {
        return ret;
    }

    ret = dw1000_gpio_init();
    if (ret < 0) {
        return ret;
    }

    dw1000_reset();

    if (DWT_DEVICE_ID != dwt_readdevid()) {
        printk("DW1000 ID error.\n");
        return -ENODEV;
    }

    if (dwt_initialise(DWT_LOADNONE) == DWT_ERROR) {
        printk("DW1000 init failed\n");
        return -ENODEV;
    }

    printk("DW1000 init done.\n");

    /* Configure DW1000. See NOTE 3 below. */
    dwt_configure(&config);

    return 0;
}

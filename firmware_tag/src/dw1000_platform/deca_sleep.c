/*
 * Copyright (c) 2020 jindongsheng1024@163.com
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>

void deca_sleep(unsigned int time_ms)
{
    k_msleep(time_ms);
}


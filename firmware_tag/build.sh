#!/bin/bash
version=0.0.0+1
build_cmd=$1

base="${BASH_SOURCE[0]}"
opt=""
run_base=$( builtin cd "$( dirname "$base" )" && pwd ${opt})
export ZEPHYR_MODULES=$( builtin cd $run_base && cd ../../ && pwd ${opt})

if [ "$build_cmd" = 'flash' ]; then
    west flash -r jlink
elif [ "$build_cmd" = 'debug' ]; then
    west debug
elif [ "$build_cmd" = 'menuconfig' ]; then
    cd build
    ninja menuconfig
elif [ "$build_cmd" = 'clean' ]; then
    rm -rf build
else
    west build -b uwb_tag ./
fi


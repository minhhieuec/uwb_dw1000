/*
 * Copyright (c) 2020 jindongsheng1024@163.com
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <zephyr.h>
#include <device.h>
#include <drivers/gpio.h>
#include <drivers/sensor.h>

void main(void)
{
    printk("hello uwb anchor\n");

    while (1)
    {
        printk("\nanchor running\n");
        k_msleep(5000);
    }
}

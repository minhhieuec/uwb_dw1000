/*
 * Copyright (c) 2020 jindongsheng1024@163.com
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <string.h>
#include <device.h>
#include <drivers/spi.h>
#include <drivers/gpio.h>

#include "dw1000.h"
#include "deca_device_api.h"
#include "deca_regs.h"

/* Buffer to store received frame. See NOTE 1 below. */
#define FRAME_LEN_MAX 127
static uint8 rx_buffer[FRAME_LEN_MAX];

#if DW1000_LP_MODE
/* Default communication configuration. See NOTE 1 below. */
static dwt_config_t config = {
    2,                   /* Channel number. */
    DWT_PRF_16M,         /* Pulse repetition frequency. */
    DWT_PLEN_1024,       /* Preamble length. Used in TX only. */
    DWT_PAC16,           /* Preamble acquisition chunk size. Used in RX only. */
    3,                   /* TX preamble code. Used in TX only. */
    3,                   /* RX preamble code. Used in RX only. */
    0,                   /* 0 to use standard SFD, 1 to use non-standard SFD. */
    DWT_BR_6M8,          /* Data rate. */
    DWT_PHRMODE_STD,     /* PHY header mode. */
    (1024 + 1 + 8 - 16)  /* SFD timeout (preamble length + 1 + SFD length - PAC size). Used in RX only. */
};
#else
/* Default communication configuration. We use here EVK1000's default mode (mode 3). */
static dwt_config_t config = {
    2,               /* Channel number. */
    DWT_PRF_64M,     /* Pulse repetition frequency. */
    DWT_PLEN_1024,   /* Preamble length. Used in TX only. */
    DWT_PAC32,       /* Preamble acquisition chunk size. Used in RX only. */
    9,               /* TX preamble code. Used in TX only. */
    9,               /* RX preamble code. Used in RX only. */
    1,               /* 0 to use standard SFD, 1 to use non-standard SFD. */
    DWT_BR_110K,     /* Data rate. */
    DWT_PHRMODE_STD, /* PHY header mode. */
    (1025 + 64 - 32) /* SFD timeout (preamble length + 1 + SFD length - PAC size). Used in RX only. */
};
#endif /* DW1000_LP_MODE */

static const struct device *g_dw1000_spi_dev = NULL;
static struct spi_config g_dw1000_spi_config;

static const struct device *g_dw1000_reset_dev = NULL;
#define DW1000_RST_PORT   "GPIOA"
#define DW1000_RST_PIN    11

static const struct device *g_dw1000_irq_dev = NULL;
static struct gpio_callback g_dw1000_irq_cb;
#define DW1000_IRQ_PORT   "GPIOA"
#define DW1000_IRQ_PIN    10

static int dw1000_spi_init(void)
{
    g_dw1000_spi_dev = device_get_binding("SPI_2");
    if (!g_dw1000_spi_dev) {
        printk("Can't fined SPI_1\n");
        return -ENODEV;
    }

    g_dw1000_spi_config.frequency = 1000000;
    g_dw1000_spi_config.operation = SPI_OP_MODE_MASTER |
        SPI_WORD_SET(8) | SPI_TRANSFER_MSB;
    g_dw1000_spi_config.slave = 0;
    g_dw1000_spi_config.cs = NULL;

    return 0;
}

int dw1000_spi_write(uint8_t *buf, uint32_t len)
{
    const struct spi_config *spi_cfg = &g_dw1000_spi_config;
    const struct spi_buf tx_buf = {
        .buf = buf,
        .len = len,
    };
    const struct spi_buf_set tx = {
        .buffers = &tx_buf,
        .count = 1
    };

    if (spi_write(g_dw1000_spi_dev, spi_cfg, &tx)) {
        return -EIO;
    }

    return 0;
}

int dw1000_spi_read(uint8_t *tx_buf_ptr, uint32_t tx_len, uint8_t *rx_buf_ptr, uint32_t rx_len)
{
    const struct spi_config *spi_cfg = &g_dw1000_spi_config;
	const struct spi_buf tx_buf = {
			.buf = tx_buf_ptr,
			.len = tx_len,
	};
	const struct spi_buf_set tx = {
		.buffers = &tx_buf,
		.count = 1
	};
	const struct spi_buf rx_buf[2] = {
		{
			.buf = NULL,
			.len = tx_len,
		},
		{
			.buf = rx_buf_ptr,
			.len = rx_len,
		}
	};
	const struct spi_buf_set rx = {
		.buffers = rx_buf,
		.count = 2,
	};

	if (spi_transceive(g_dw1000_spi_dev, spi_cfg, &tx, &rx)) {
		return -EIO;
	}

    return 0;
}

void dw1000_sleep(uint32_t time_ms)
{
    k_msleep(time_ms);
}

static void dw1000_isr_handler(const struct device *dev,
        struct gpio_callback *cb, uint32_t pins)
{
    dwt_isr();
}

static int dw1000_gpio_init(void)
{
    int ret;

    g_dw1000_reset_dev = device_get_binding(DW1000_RST_PORT);
    if (!g_dw1000_reset_dev) {
        printk("Can't fined %s\n", DW1000_RST_PORT);
        return -ENODEV;
    }

    ret = gpio_pin_configure(g_dw1000_reset_dev, DW1000_RST_PIN, GPIO_OUTPUT_ACTIVE);
    if (ret < 0) {
        return ret;
    }

    g_dw1000_irq_dev = device_get_binding(DW1000_IRQ_PORT);
    if (!g_dw1000_irq_dev) {
        printk("Can't fined %s", DW1000_IRQ_PORT);
    }

    ret = gpio_pin_configure(g_dw1000_irq_dev, DW1000_IRQ_PIN, GPIO_INPUT);
    if (ret < 0) {
        return ret;
    }

    ret = gpio_pin_interrupt_configure(g_dw1000_irq_dev, DW1000_IRQ_PIN, GPIO_INT_EDGE_TO_ACTIVE);
    if (ret < 0) {
        return ret;
    }

    gpio_init_callback(&g_dw1000_irq_cb, dw1000_isr_handler,
            BIT(DW1000_IRQ_PIN));

    gpio_add_callback(g_dw1000_irq_dev, &g_dw1000_irq_cb);

    printk("dw1000 gpio init done.\n");

    return 0;
}

void dw1000_reset(void)
{
    k_usleep(10);
    gpio_pin_set(g_dw1000_reset_dev, DW1000_RST_PIN, 0);
    k_usleep(10);
    gpio_pin_set(g_dw1000_reset_dev, DW1000_RST_PIN, 1);
    k_sleep(K_MSEC(2000));
}

static void dw1000_rx_ok_cb(const dwt_cb_data_t *cb_data)
{
    printk("dw1000 app callback.\n");

    /* Perform manual RX re-enabling. See NOTE 5 below. */
    dwt_rxenable(DWT_START_RX_IMMEDIATE | DWT_NO_SYNC_PTRS);

    if (cb_data->datalength <= FRAME_LEN_MAX)
    {
        dwt_readrxdata(rx_buffer, cb_data->datalength, 0);
    }

    printk("rx_len:%d\n", cb_data->datalength);
    for (int i = 0; i < cb_data->datalength; i++) {
        printk("%x ", rx_buffer[i]);
    }
    printk("\n");
}

static void dw1000_rx_err_cb(const dwt_cb_data_t *cb_data)
{
    /* Re-activate reception immediately. */
    dwt_rxenable(DWT_START_RX_IMMEDIATE);
    printk("\n----->dw1000_rx_err\n");
}

int dw1000_init(void)
{
    int ret;

    ret = dw1000_spi_init();
    if (ret < 0) {
        return ret;
    }

    ret = dw1000_gpio_init();
    if (ret < 0) {
        return ret;
    }

    dw1000_reset();

    if (DWT_DEVICE_ID != dwt_readdevid()) {
        printk("DW1000 ID error.\n");
        return -ENODEV;
    }

    if (dwt_initialise(DWT_LOADNONE) == DWT_ERROR) {
        printk("DW1000 init failed\n");
        return -ENODEV;
    }

    printk("DW1000 init done.\n");

    /* Configure DW1000. See NOTE 3 below. */
    dwt_configure(&config);

    /* Activate double buffering. */
    dwt_setdblrxbuffmode(1);

    /* Register RX call-back. */
    dwt_setcallbacks(NULL, &dw1000_rx_ok_cb, NULL, &dw1000_rx_err_cb);

    /* Enable wanted interrupts (RX good frames and RX errors). */
    dwt_setinterrupt(DWT_INT_RFCG | DWT_INT_RPHE | DWT_INT_RFCE | DWT_INT_RFSL | DWT_INT_SFDT, 1);

    /* Activate reception immediately. See NOTE 3 below. */
    dwt_rxenable(DWT_START_RX_IMMEDIATE);

    return 0;
}

static void dw1000_thread(void *arg, void *arg1, void *arg2)
{
    dw1000_init();

    while (1)
    {
        k_msleep(100);
        continue;
    }
}

#define DW1000_STACK_SIZE  2048
#define DW1000_PRIORITY    5
K_THREAD_DEFINE(dw1000_thid, DW1000_STACK_SIZE,
        dw1000_thread, NULL, NULL, NULL,
        DW1000_PRIORITY, 0, 0);

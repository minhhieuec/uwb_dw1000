/*
 * Copyright (c) 2020 jindongsheng1024@163.com
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "dw1000.h"

void deca_sleep(unsigned int time_ms)
{
    dw1000_sleep(time_ms);
}


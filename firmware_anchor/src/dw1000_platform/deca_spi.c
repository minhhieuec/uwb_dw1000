/*
 * Copyright (c) 2020 jindongsheng1024@163.com
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <errno.h>
#include <string.h>

#include "deca_types.h"
#include "deca_device_api.h"
#include "dw1000.h"

static uint8_t g_dw1000_spi_buffer[5000];

int writetospi(uint16 headerLength,
               const uint8 *headerBuffer,
               uint32 bodyLength,
               const uint8 *bodyBuffer)
{
    int ret;
    decaIrqStatus_t stat;
    uint32_t len = headerLength + bodyLength;

    if (len >= sizeof(g_dw1000_spi_buffer)) {
        return -EINVAL;
    }

    memcpy(g_dw1000_spi_buffer, headerBuffer, headerLength);
    memcpy(&g_dw1000_spi_buffer[headerLength], bodyBuffer, bodyLength);

    stat = decamutexon();

    ret = dw1000_spi_write(g_dw1000_spi_buffer, len);

    decamutexoff(stat);

    return ret;
}

int readfromspi(uint16 headerLength,
                const uint8 *headerBuffer,
                uint32 readlength,
                uint8 *readBuffer)
{
    int ret;
    decaIrqStatus_t stat;

    stat = decamutexon();

    ret = dw1000_spi_read((uint8_t *)headerBuffer, headerLength, readBuffer, readlength);

    decamutexoff(stat);

    return ret;
}


/*
 * Copyright (c) 2020 jindongsheng1024@163.com
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _DW1000_SPI_H
#define _DW1000_SPI_H

#include <stdint.h>

#define DW1000_LP_MODE   1

int dw1000_init(void);

int dw1000_spi_write(uint8_t *buf, uint32_t len);

int dw1000_spi_read(uint8_t *tx_buf_ptr, uint32_t tx_len, uint8_t *rx_buf_ptr, uint32_t rx_len);

void dw1000_sleep(uint32_t time_ms);

#endif /* _DW1000_SPI_H */

